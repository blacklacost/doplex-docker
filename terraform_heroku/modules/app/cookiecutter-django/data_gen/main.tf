data "external" "data_gen" {
  program = ["python", "${path.module}/data_gen.py"]
}

output "admin_url" {
  value = data.external.data_gen.result.admin_url
}

output "secret_key" {
  value = data.external.data_gen.result.secret_key
}
