variable "app_name" {
  description = "Project Name"
  default = "doplex-docker"
}

variable "buildpacks" {
  description = "Heroku buildpacks"
  type = list
  default = [
    "heroku/nodejs",
    "https://github.com/heroku/heroku-buildpack-python",
  ]
}

terraform {
  backend "remote" {
    organization = "blacklacost"
    workspaces {
      name = "heroku-doplex-docker"
    }
  }
}

module "app-stage" {
  source = "./modules/stage"

  app_name = var.app_name
  buildpacks = var.buildpacks
}

module "app-prod" {
  source = "./modules/prod"

  app_name = var.app_name
  buildpacks = var.buildpacks
}
